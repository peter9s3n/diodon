var mongoose = require('mongoose');
var db = mongoose.model('microcomputer');
var moment = require('moment-timezone');

var record = {
    get: (req, res, next) => {
        db.find((err, result) => {
            if(err){
                console.log('DB query error');
                return false;
            }else{
                res.send(JSON.stringify(result));
            }
        })
    },
    post: (req, res, next) => {
        /*
            type req.payload
        */
        var time = moment().tz('Asia/Taipei').format('hh:mm:ss');
        var set = {};
        set['q' + msg.q] = time;
        msg.time = time;
        db.findOneAndUpdate((err, result) => {
            if(err){
                console.log('DB query error');
                res.status(500).send('writing db error');
            }else{
                res.send(JSON.stringify(result));
            }
        })
    },
    delete: () => {},
    put: () => {}
};

module.exports = record;