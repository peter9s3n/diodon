var about = {
    get: (req, res, next) => {
        if (req.session.logined){
            res.render('about', {login: true});
        }else{
            res.render('about', {login: false});
        }
    }
};

module.exports = about;