var admin_accounts = require('../../config/admin_accounts');

var auth = {
    isAuth: (req, res, next) => {
        if (!req.session.logined) {
            res.redirect('login');
        } else {
            next();
        }
    },
    get: (req, res, next) => {
        res.render('login');
    },
    post: (req, res, next) => {
        if ((req.body.form_email in admin_accounts) && admin_accounts[req.body.form_email].password == req.body.form_password){
            req.session.logined = true;
            req.session.admin = req.body.form_email;
            res.render('index');
        }else{
            res.redirect('login');
        }
    }
};

module.exports = auth;