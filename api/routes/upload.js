var fs = require('fs');
var es = require("event-stream");
var formidable = require('formidable');
var mongoose = require('mongoose');
var microcomputer = mongoose.model('microcomputer');

var upload = {
    get: (req, res, next) => {
        res.render('upload');
    },
    post: (req, res, next) => {
        var u_file;
        var form = new formidable.IncomingForm();
        
        microcomputer.remove({}, (err) => {
            if(err){
                console.log('remove collections error! ' + err)
                return false;
            }
            console.log('remove collection successful');
        });
        console.log(JSON.stringify(form));
        form.encoding = 'utf-8';
        form.uploadDir = './upload/';
        form.keepExtensions = true;
        form.maxFieldsSize = 2 * 1024 * 1024;
        form.parse(req, (err, fields, file) => {
            /*if(file.student_list.type != 'text/plain'){
                console.log('file type error: ' + file.student_list.type);
                return false;
            }*/
            
            console.log('file: ' + file.student_list.path);
            
            var s = fs.createReadStream(file.student_list.path).pipe(es.split()).pipe(es.mapSync( (line) => {
                //console.log('line: ' + line);
                var index = line.search('\t');
                var name = line.slice(0,index);
                var id = line.slice(index + 1);  // +1 避免Tab 
                var data_insert = new microcomputer({name: name, id: id, q1: '', q2: '', q3: '', q4: ''});
                data_insert.save( (err) => {
                    if(err){
                        console.log('db insert error! ' + err);
                        return false;
                    }
                    console.log('index: ' + index + ' name: ' + name + ' id: ' + id);
                });
            }))
            
        });
        res.status(200).send();
    },
};

module.exports = upload;