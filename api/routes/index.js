var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var microcomputer = mongoose.model('microcomputer');

var auth = require('./auth');
var about = require('./about');
var record = require('./record');
var upload = require('./upload');

router.get('/about', about.get);

router.get('/login', auth.get);
router.post('/login', auth.post);

// main show page by projector
router.get('/', auth.isAuth, (req, res, next) => {
    res.render('index');
});

router.get('/record', auth.isAuth, record.get);
router.post('/record', auth.isAuth, record.post);
router.delete('/record', auth.isAuth, record.delete);
router.put('/record', auth.isAuth, record.put);

router.get('/upload', auth.isAuth, upload.get);
router.post('/upload', auth.isAuth, upload.post);


// TA update page
router.get('/ta_pannel', auth.isAuth,(req, res, next) => {
    microcomputer.find().
    sort('id').
    exec((err, result) => {
        if(err){
            console.log('microcomputer DB query error');
            return false;
        }else{
            res.status(200).send({data: result}); 
        }
    });
});

// test page
router.get('/test', auth.isAuth, (req, res, next) => {
    res.render('test');
});


module.exports = router;