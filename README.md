# Diodon #

### What is Diodon app? ###

* A general account which allow TA use basic auth to login.
* TA can upload the ID-Name list, it's easy to use.
* RWD webpage can fit the resolution bellow 1024.
* Mobile manifeast is designed for TA page which providing convenience control.
* [about us](http://bnlab.ce.ncu.edu.tw/)

### User guidelines ###

* Go to the [website](http://140.115.152.223:3000/)
* Login with defalt account/password.
* TA page provides convenience control.

### Developer guidelines ###

* Start the mongodb

```sh
# mongod --dbpath ./db
```

* Start the nodejs

```sh
# npm start
```

### Contribution guidelines ###

* Login Feature
* Socket-io 
* Backend MongoDB
* Tests
* Code review

### Future works ###

* When TA submits , now can pop a notification to show success or not. **Show submit result in future.**
* A pure show page(not listen onClick event) is needed in index.
* Need an administrator page to roll-back error submissions.
* Need an administrator page to submit specific variables.(Time, ID, question)
    
  > In ```TMP``` directory, tmp_insert.js is a temporary version for this feature. Edit the file with variables to submit, then run ```node tmp_insert.js```. Should have a specific administrator page for this feature in the future.

* Delete isn't not real delete but only update with `time=NULL`. **Use Mongodb API to delete in future**
* Show page need to rearrange the font-size which bellow 1024px(should be larger).

### Developers ###

* Yu-Hsuan Lin
* Chia-Ray Lin