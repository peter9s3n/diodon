var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var microcomputer = mongoose.model('microcomputer');

var auth = require('./auth');

router.get('/about', (req, res, next) => {
    if (req.session.logined){
        res.render('about', {login: true});
    }else{
        res.render('about', {login: false});
    }
});

router.get('/login', auth.get);
router.post('/login', auth.post);

// main show page by projector
router.get('/', auth.isAuth, (req, res, next) => {
    res.render('index');
});

router.get('/record', auth.isAuth, (req, res, next) => {
    res.send('TODO');
});

router.get('/upload', auth.isAuth, (req, res, next) => {
    res.render('upload');
});

router.get('/ta_pannel', auth.isAuth,(req, res, next) => {
    res.render('ta_pannel');
});

router.get('/test', auth.isAuth, (req, res, next) => {
    res.render('test');
});

module.exports = router;